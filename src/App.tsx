import React from 'react';
import logo from './logo.svg';
import Header from './components/header/header';
import './App.css';
import Footer from './components/footer/footer';
import OrderList from './components/order-list-section/order-list';
import ProfilesAndAccessories from './components/profiles-and-accessories-section/profiles-and-accessories';
import OurAdventages from './components/our-adventages/our-adventages';
import OurWork from './components/our-work-section/our-work';
import Form from './components/form-section/form';
import { Outlet, Routes, Route } from 'react-router-dom';
function App() {
  return (
    <div className="App">
        <Routes>
          <Route path='/' element={<Layout/>}>
            <Route index element={<Home />} />
            <Route path="contact" element={<Contact />}></Route>
          </Route>
        </Routes>
    </div>
  );
}

function Layout() {
  return (<>
    <Header />
    <Outlet />
    <Footer />
  </>)
}

function Home() {
  return (<>
      <OrderList/>
      <ProfilesAndAccessories/>
      <OurAdventages/>
      <OurWork/>
  </>)
}

function Contact() {
  return (<>
    <Form/>
  </>)
}

export default App;
