import './profiles-and-accessories.scss';
import GermanyFirstImage from '../../assets/geormany1.jpg'
import AustriaFirstImage from '../../assets/Austria1.jpg'
import GermanySecondImage from '../../assets/Germany2.jpg'
import AustriaSecondImage from '../../assets/Austria2.jpg'
import RussiaFirstImage from '../../assets/Russia1.jpg'
import RussiaSecondImage from '../../assets/Russia2.jpg'
import { useState } from 'react'


type CountryItem = {
    label: string;
    id: string | number;
    images: string[];
    description: string;
}

export default function ProfilesAndAccessories() {
    const countryTabs: CountryItem[] = [
        {
            label: 'Германия',
            id: '1',
            images: [GermanyFirstImage, GermanySecondImage],
            description: `Герма́ния официальное название — Федерати́вная Респу́блика Герма́ния Расположенная
            в центре Европы Германия омывается водами Балтийского и Северного морей. Граничит с
            Данией на севере, Польшей и Чехией на востоке, Австрией и Швейцарией на юге, Францией, Люксембургом, Бельгией и Нидерландами на западе.
            По государственному устройству является федеративным государством
            в составе 16 субъектов — федеральных земель (Бавария, Баден-Вюртемберг, Берлин,
            Бранденбург, Бремен, Гамбург, Гессен, Мекленбург-Передняя Померания, Нижняя Саксония, Рейнланд-Пфальц, Саар, Саксония, Саксония-Анхальт, Северный Рейн — Вестфалия, Тюрингия, Шлезвиг-Гольштейн). Форма государственного правления — парламентская республика. Пост Федерального канцлера ФРГ с 8 декабря 2021 года занимает Олаф Шольц (СДПГ)[12], с 19 марта 2017 года должность Федерального президента ФРГ занимает Франк-Вальтер Штайнмайер (СДПГ), который выполняет представительские функции в стране.
            Столица — Берлин[13]. Официальный язык — немецкий
            Германия — страна с динамично развивающейся экономикой. Объём ВВП за 2017 год составил 3,685 триллиона долларов США (около 44 550 долларов США на душу населения). Денежная единица — евро[14].`,
        },
        {
            label: 'Австрия',
            id: '2',
            images: [AustriaFirstImage, AustriaSecondImage],
            description: `А́встрия официальное название — Австри́йская Респу́блика  государство в Центральной Европе.
            Население составляет 8,8588 млн человек (2019), площадь — 83 879 км²[7].
            Занимает 98-е место в мире по численности населения и 113-е по территории.
            Столица — Вена. Государственный язык — немецкий.`
        },
        {
            label: 'Россия',
            id: '3',
            images: [RussiaFirstImage, RussiaSecondImage],
            description: `Росси́я, или Росси́йская Федера́ция  государство в Восточной Европе и Северной Азии.
            Россия — крупнейшее государство в мире, её территория в конституционных границах,
            с территорией большей части Крыма, присоединение которого не получило международного признания, составляет
            Столица — Москва. Государственный язык на всей территории страны — русскийПерейти к разделу
            «#Языки», в ряде регионов России также установлены свои государственные и официальные языки.
            Денежная единица — российский рубльПерейти
            к разделу «#Общее состояние, основные показатели».`
        }
    ]
    const [activeTab, setActiveTab] = useState<number | string>(countryTabs[0].id);
    function onTabChange(id: string | number): void {
        setActiveTab(id)
    }
    return (<section className="profiles-and-accessories-section">

        <div className="profiles-and-accessories-text">
            Только проверенные профили и комплектующие
        </div>
        <div className="container _vertical">
            <div className="link-country-position">
                {countryTabs.map(country => (
                    <button onClick={() => onTabChange(country.id)} className={['link-country', 'text-link-country', country.id === activeTab ? 'active' : ''].join(' ')}>{country.label}</button>
                ))}
            </div>
            {countryTabs.map(country => {
                return country.id === activeTab && (
                    <div className="picture-text-block active">
                        <div className="vertical-img">
                            {country.images.map(image => (
                                <div className="vertical-img-top" >
                                    <img className="img-country" src={image} alt={country.label} />
                                </div>
                            ))}
                        </div>
                        <div className="big-text-block">
                            <p> 
                                {country.description}
                            </p> 
                        </div>
                    </div>

                )
            })}
        </div>
    </section>
    )
}