import './order-list.scss' 
import FirstImageFirstSlider from '../../assets/for-slider1(2).jpg'
import SecondImageFirstSlider from '../../assets/for-slider1.jpg'
import ThirdImageFirstSlider from '../../assets/for-slider1(3).jpg'
import FourthImageFirstSlider from '../../assets/for-slider1(4).jpg'
import FifthImageFirstSlider from '../../assets/for-slider1(5).jpg'
import SixthImageFirstSlider from '../../assets/for-slider1(6).jpg'
import SeventhImageFirstSlider from '../../assets/for-slider1(7).jpg'
import EighthImageFirstSlider from '../../assets/for-slider(8).jpg'
import NinthImageFirstSlider from '../../assets/for-slider(9).jpg'
import TenthImageFirstSlider from '../../assets/for-slider1(10).jpg'
import CombinedShapeRight from '../../assets/Combined-Shape-right.png'
import CombinedShapeLeft from '../../assets/Combined-Shape-left.png'
import { useState } from 'react'
type SliderItem = {
  id: string | number;
  arrImg: string[]
}
export default function OrderList(){
  const slider: SliderItem[]=[
    {
     id:'1',
     arrImg:[FirstImageFirstSlider,SecondImageFirstSlider]
    },
    {
     id: '2',
     arrImg:[ThirdImageFirstSlider,FourthImageFirstSlider]
    },
    {
      id: '3',
      arrImg:[FifthImageFirstSlider,SixthImageFirstSlider]
    },
    {
      id:'4',
      arrImg:[SeventhImageFirstSlider,EighthImageFirstSlider]
    },
    {
      id: '5',
      arrImg:[NinthImageFirstSlider,TenthImageFirstSlider]
    }
  ]
  const [activeTab, setActiveTab] = useState <number>(0);
  const [ trackProgress, setTrackProgress] = useState <string>('0%');
  function onTabChange(index: number): void {
      if(index > slider.length - 1) {
        index = 0;
      }

      if(index < 0) {
        index = slider.length - 1;
      }
      
      setTrackProgress( (index / slider.length) * 100  + '%');

      setActiveTab(index);
  }
return (

<section className="order-list">
<div className="container">  
  <div className="order-consultation">
      <div className="servis-list main">
       <p>
           Пластиковые окна и двери
        </p>
        <p className='decoration-text-order'>
        Отделка балконов
        </p>
        
      </div>
      <div className="servis-list midle">
        <p className='decoration-text-order'>
            Уникальное торговое предложение
        </p>
        <p className='decoration-text-order'>
            несколько строчек
        </p>
      </div>
      <div className="link-consultation"> 
      <a href="#">  <p className="position-text-consultation">Заказать консультацию</p></a>
      </div>  
  </div> 
<div className="slider-show">
  <div className="slider">
    {slider.map((slide, index) => {
        return activeTab === index 
          ? (<div className='group' key={slide.id}>
              {slide.arrImg.map(image => (
                <div  className="head-image-slider-left">
                  <img className="img-slider-order-list" src={image} alt="картинка" />
                </div>
              ))}
          </div>)
          : undefined;
    })}
  </div>

</div> 
</div>
<div className="container _vertical">
<div className="progress-bar">
<div className="button-slider-left">
    <button onClick={() => onTabChange(activeTab - 1)}> <img src={CombinedShapeLeft} alt="Кнопка" /></button>
</div>
<div  className="button-slider-right">
 <button onClick={() => onTabChange(activeTab + 1)}><img src={CombinedShapeRight} alt="кнопка" /></button> 
</div>
<div className="progress-control">
<div className="progress-sli" style={{left: trackProgress}}>
  
</div>
<div className="progress-line">

</div>
</div>
</div>
</div>
</section>)

}