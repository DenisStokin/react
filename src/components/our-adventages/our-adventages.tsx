import './our-adventages.scss'
import ImageFirstAdventages from '../../assets/adventages1.jpg'
import ImageSecondAdventages from '../../assets/adventages2.jpg'
import ImageThirdAdventages from '../../assets/adventages3.jpg'
import ImageFourthAdventages from '../../assets/adventages4.jpg'
import ImageFifthAdventages from '../../assets/adventages5.jpg'
import ImageSixthAdventages from '../../assets/adventages6.jpg'
import { useState } from 'react'

type AdvantageItem = {
    label: string;
    id: string | number;
    image: string;
}

export default function Advantages() {
    const advantagesTabs: AdvantageItem[] = [
        {
            label: 'Преимущество 1',
            id: '1',
            image: ImageFirstAdventages
        },
        {
            label: 'Преимущество 2',
            id: '2',
            image: ImageSecondAdventages
        },
        {
            label: 'Преимущество 3',
            id: '3',
            image: ImageThirdAdventages
        },
        {
            label: 'Преимущество 4',
            id: '4',
            image: ImageFourthAdventages
        },
        {
            label: 'Преимущество 5',
            id: '5',
            image: ImageFifthAdventages
        },
        {
            label: 'Преимущество 6',
            id: '6',
            image: ImageSixthAdventages
        }

    ]

    const [activeTab, setActiveTab] = useState<number | string>(advantagesTabs[0].id);

    function onTabChange(id: string | number): void {
        setActiveTab(id)
    }

    return (
        <section className="our-advantages">
            <div className="container _vertical">
                <div className="our-advantages-text">
                    Наши преимущества
                </div>
                <div className="advantages-block">
                    <div className="advantages-numeration">
                        {advantagesTabs.map(advantage => (
                            <button className={['advantages', advantage.id === activeTab ? 'active' : ''].join(' ')} onClick={() => onTabChange(advantage.id)}>
                                {advantage.label} 
                            </button>
                        ))}
                    </div>
                    <>{advantagesTabs.map(advantage => {
                        return activeTab === advantage.id
                            ? (
                                <div className={'image-advantages active'}>
                                    <img className="style-img-advantages" src={advantage.image} alt={advantage.label} />
                                </div>
                            )
                            : undefined;
                    })}
                    </>
                </div>
            </div>
        </section>
    )
}