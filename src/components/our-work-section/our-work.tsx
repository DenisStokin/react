import './our-work.scss'
import { useState } from 'react'
type WorkItem = {
    label: string;
    id: string | number;
    descriptions: string[]
}
export default function OurWork(){
    const workTabs: WorkItem[] = [
        {
            label: 'Окна',
            id: '1',
            descriptions:['окна','окна','окна','окна']
        },
        {
            label: 'Двери',
            id: '2',
            descriptions:['Двери','Двери','Двери','Двери']
        },
        {
            label: 'Балконы',
            id: '3',
            descriptions:['Балконы','Балконы','Балконы','Балконы']
        },
        {
            label: 'Жалюзи',
            id: '4',
            descriptions:['Жалюзи','Жалюзи','Жалюзи','Жалюзи']
        },
        {
            label: 'Рольставни',
            id: '5',
            descriptions:['Рольставни','Рольставни','Рольставни','Рольставни']
        }
    ]
    const [activeTab, setActiveTab] = useState <number | string>(workTabs[0].id);
    function onTabChange(id: string | number): void {
        setActiveTab(id)
    }
    return (
        <section className="our-work-section">
            <div className='container _vertical'>
   <div className="our-work">
             Наши работы
           </div>
           <div className="link-furniture">
           {workTabs.map(work=>(
           <p onClick={() => onTabChange(work.id)} className={['text-link-furniture',  work.id === activeTab ? 'active' : ''].join(' ')}>{work.label}</p>))}
           </div>

                <>{workTabs.map(work=>{
                return work.id===activeTab && (<div className="our-work-block active" id="tab-10">
            <div className="wrpapper">
                {work.descriptions.map(description=>(
                    <>
                    
                            <div className="block-litle-image">
                                <div className="litle-image-work">{description}</div>
                            </div>
                           

                        
                    </>
                  
                ))}
                
            </div>
            <div className="block-big-image">
            {work.label}
            </div>
            </div>
               )})}
            </>
           


         
         
       
           

     

          
           </div>
           
        </section>
    )
}