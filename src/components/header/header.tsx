import { Link } from 'react-router-dom';
import './header.scss';

export default function Header() {
    return (
        <header>

        <div className="container">
            <Link className="logo" to='/'>logo</Link>
            <nav className="header-menu" >
                <Link className="style-link-header" to="/"> Главная</Link>
                <Link className="style-link-header" to="/">О нас</Link>
                <Link className="style-link-header" to="contact">Вопросы</Link>
            </nav>
            <div className="contacts-header">
                <a className="position-number contact-number" href="tel:+89257611616">
                    8 (925) 761-16-16
                </a>
                <p className="contact-email">
                    impost-okna@mail.ru
                </p>
            </div>
        </div>
    </header>
    )
}